Summary:        Nigel's performance Monitor for Linux 
Name:           nmon
Version:        16n
Release:        5%{?dist}
License:        GPLv3
URL:            http://nmon.sourceforge.net
Source0:        https://sourceforge.net/projects/%{name}/files/lmon%{version}.c
Source1:        https://sourceforge.net/projects/%{name}/files/Documentation.txt
Source2:        %{name}.1

BuildRequires:  gcc ncurses-devel

%description
nmon is a systems administrator, tuner, benchmark tool, which provides information
about CPU, disks, network, etc., all in one view.

%prep
%setup -T -c -n %{name}
cp %{SOURCE0} .
cp %{SOURCE1} .
sed -i "s/\r//g"  Documentation.txt

%build
%ifarch ppc64le
  %{__cc} %{optflags} -D JFS -D GETUSER \
     -D LARGEMEM -lncurses -lm lmon%{version}.c -D POWER -o %{name}
%else
  %{__cc} %{optflags} -D JFS -D GETUSER \
     -D LARGEMEM -D X86 -lncurses -lm lmon%{version}.c -o %{name}
%endif

%install
install -D -p -m 0755 %{name} %{buildroot}%{_bindir}/%{name}
install -D -p -m 0644 %{SOURCE2} %{buildroot}%{_mandir}/man1/%{name}.1

%files
%doc Documentation.txt 
%{_bindir}/%{name}
%{_mandir}/man1/%{name}.1.*

%changelog
* Fri Aug 16 2024 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 16n-5
- Rebuilt for loongarch release

* Fri Sep 08 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 16n-4
- Rebuilt for OpenCloudOS Stream 23.09

* Fri Apr 28 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 16n-3
- Rebuilt for OpenCloudOS Stream 23.05

* Fri Mar 31 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 16n-2
- Rebuilt for OpenCloudOS Stream 23

* Fri Dec 23 2022 Shuo Wang <abushwang@tencent.com> - 16n-1
- initial build
